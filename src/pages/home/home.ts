import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first/first';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  btnClicked(): void {
    console.log("Button is clicked.");
  }

  gotoFirstPage(): void {
    this.navCtrl.push(FirstPage, {nama: "Christine", umur: "21"});
  }

}
